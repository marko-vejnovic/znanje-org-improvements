<?php
include("db_config.php");
session_start();

if (!isset($_SESSION["user-name"]))
{
	header("location:login.php");
	die();
}

// Check if user with that username exists
$pdo = connectToDB();
$stmt = $pdo->prepare("SELECT * FROM test_results WHERE user_name = ?");
$stmt->execute([$_SESSION["user-name"]]);
$profile = $stmt->fetch();

if (empty($profile))
{
	header("location:login.php");
	die();
}

// Calculate how well the tests are done
// This should be enums in a sql db, but I'm too lazy
$result["question-1"] = ($_POST["question-1"] == 1) ? 1 : 0;
$result["question-2"] = ($_POST["question-2"] == 3) ? 1 : 0;

$pdo = connectToDB();
$stmt = $pdo->prepare("UPDATE `test_results` SET `question_1` = ?, `question_2` = ? WHERE `user_name` = '" . $profile["user_name"] . "'");
$success = $stmt->execute([$result["question-1"], $result["question-2"]]);

if (!$success)
{
	header("location:uhoh.html");
	die();
}

header("location:done.php");
die();

?>