You probably shouldn't be here :(

# Installation
1. Make sure you have a fully working LAMP stack (XAMPP, if necessary, avoid if possible)
2. The HTTP files are in `htdocs/`
3. Create an empty database with the name 'zelgrb'.
4. Import into the database using `mysql -u username -p zelgrb < zelgrb.sql`
5. Configure your parameters in `htdocs/db_config.php`.
