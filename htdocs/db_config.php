<?php
	function connectToDB()
	{
		$DB_SERVER = "localhost";
		$USER = "root";
		$PASSWORD = "";
		$DB_NAME = "zelgrb";
		$DB_CHARSET = "utf8";

		$pdo_dsn = "mysql:host=$DB_SERVER;dbname=$DB_NAME;charset=$DB_CHARSET";
		$pdo_options = [
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES => true
		];

		$pdo = new PDO($pdo_dsn, $USER, $PASSWORD, $pdo_options);

		return $pdo;
	}
?>