<?php
include("db_config.php");

// There should be some sort of login, but I'm too lazy
?>

<!DOCTYPE html>
<html>
<head>
	<title>Pregled</title>
</head>
<body>

<?php
// Fetch data
$pdo = connectToDB();

$stmt = $pdo->prepare("SELECT `user_name`, `question_1`, `question_2` FROM `test_results`");
$stmt->execute();
$resultsAll = $stmt->fetchAll();
?>

<table border="1">
	<tr>
		<th>Ime Ucenika</th>
		<th>Prvo pitanje</th>
		<th>Drugo pitanje</th>
	</tr>

	<?php
		foreach ($resultsAll as $row)
		{
			echo "<tr>";

			foreach ($row as $cell)
			{
				echo "<td>" . $cell . "</td>";
			}

			echo "</tr>";
		}
	?>
</table>

</body>
</html>