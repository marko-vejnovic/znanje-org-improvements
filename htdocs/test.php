<?php
include("db_config.php");
session_start();

if (!isset($_SESSION["user-name"]))
{
	header("location:uhoh.html");
	die();
}

$pdo = connectToDB();
$stmt = $pdo->prepare("SELECT * FROM test_results WHERE user_name = ?");
$stmt->execute([$_SESSION["user-name"]]);
$profile = $stmt->fetch();

if (!empty($profile["question_1"]) || !empty($profile["question_2"]))
{
	// Questions have already been answered. Uh oh, I see tampering. Punish the student?
	header("location:uhoh.html");
	die();
}
?>

<!DOCTYPE html>
<html>
<head>

<!-- Jquery za user stvari -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

<link rel="stylesheet" type="text/css" href="main.css">
<title>Test!</title>

</head>
<body>

<form name="test-form" action="validateTest.php" method="post">
	<div class="form-group" id="form-group-1">
		<label for="question-1">Ko je napisao Linux Kernel?</label></br>
		<input type="radio" name="question-1" value="1">Linus Torvalds</br>
		<input type="radio" name="question-1" value="2">Bill Gates</br>
		<input type="radio" name="question-1" value="3">Petar Petrovic Njegos</br>

		<button type="button" id="form-group-1-button">Dalje!</button> <!-- This button does nothing, except trigger JQuery code -->
	</div>

	<div class="form-group" id="form-group-2">
		<label for="question-2">Koje su razlike izmedju SIGKILL i SIGTERM?</label></br>
		<input type="radio" name="question-2" value="1">SIGTERM nasilno ubija dok SIGKILL trazi od procesa da zakljuci threadove.</br>
		<input type="radio" name="question-2" value="2">SIGKILL ugasi program, SIGTERM otvori novi terminal istog programa.</br>
		<input type="radio" name="question-2" value="3">SIGKILL nasilno ubija dok SIGTERM trazi od procesa da zakljuci threadove.</br>

		<button type="submit" id="form-submit-button">Kraj!</button> <!-- The last button has to be of type submit -->
	</div>
</form>

<script type="text/javascript">
	window.onload = function() {
		// Hide every form group
		$(".form-group").hide();

		// On Start show first form group
		$("#form-group-1").show();
		$("#form-group-1-button").click(function() {
			// Hide first group and show second group
			$("#form-group-1").hide();
			$("#form-group-2").show();
		});
	};
</script>

</body>
</html>